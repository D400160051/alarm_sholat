/*............................................................................................................*/
#define Ihtiyat 2 // sebagai waktu ihtiyat (untuk berhati-hati)
#define buzzer 13
#include <TimeLib.h>
#include "PrayerTimes.h"

double waktu[ sizeof(TimeName)/ sizeof( char*)]; //0.00

static const char *NamaWaktu [] ={ //array untuk waktu-waktu shalat & alarm
 "Imsak" ,      //0
 "Subuh" ,      //1
 "Sunrise" ,    //2 
 "Dzuhur" ,     //3
 "Ashar" ,      //4
 "Sunset" ,     //5
 "Maghrib" ,    //6
 "Isya'"  ,      //7
 "Bangun_Subuh",  //8
 "Sedekah_Subuh", //9
 "Piket_Pagi",    //10
 "Piket_Sore",    //11
 "Waktu_Belajar", //12
 "Waktu_Tidur"    //13
};

int Sjam[14]; //penyimpanan jam sholat
int Smin[14]; //penyimpanan menit sholat

const byte JUMLAH_WAKTU = sizeof( NamaWaktu )/ sizeof( char*); // = jumlah waktu (9)

// pengaturan bujur lintang 
  float Latitude = -7.5543f ; // lintang
  float Longitude = 110.8270f ; // bujur
  int GMT = 7 ; // zona waktu WIB=7, WITA=8, WIT=9
// -6.184499, 106.827153 (jakarta pusat)
// -7.554301, 110.827064 (solo MBZ)

void setup() {
  Serial.begin(9600);

  set_calc_method (ISNA); // metode perhitungan
  set_asr_method ( Shafii); // madzhab?
  set_high_lats_adjust_method( AngleBased ); // Adjusting Methods for Higher Latitudes
  set_fajr_angle (20); // sudut waktu subuh
  set_isha_angle (18); // sudut waktu isyak

}

void loop() {
  GetPrayerTimes();
  
   for( byte i= 0 ; i<14; i++){
    Serial.print(NamaWaktu[i]);
    Serial.print(" = ");
    Serial.print(Sjam[i]);
    Serial.print(":");
    Serial.println(Smin[i]);
    delay(10);
   }
}
void jadwal_lain()
{
  // 15 menit sebelum subuh [8]
  
  Sjam[8] = Sjam[1];
  Smin[8] = Smin[1]-15;//menit subuh dikurangi 15 menit
  if(Smin[8] < 0 )
    {
      Sjam[8] = Sjam[8] - 1;
      Smin[8] = Smin[8] + 60;
    }
  // Sedekah subuh 10 menit sebelum subuh [9]
  Sjam[9] = Sjam[1];
  Smin[9] = Smin[1]-10;//menit subuh dikurangi 10 menit
  if(Smin[9] < 0 )
    {
      Sjam[9] = Sjam[8] - 1;
      Smin[9] = Smin[8] + 60;
    }
  // piket pagi 05:30 [10]
  Sjam[10] = 5;
  Smin[10] = 30;
  // piket sore 16.30 [11]
  Sjam[11] = 16;
  Smin[11] = 30;
  // waktu belajar 19:30 [12]
  Sjam[12] = 19;
  Smin[12] = 30;
  // waktu tidur 21:30  [13]
  Sjam[13] = 21;
  Smin[13] = 30;


}
void GetPrayerTimes(){
  get_prayer_times (2023, 8, 31, Latitude, Longitude, GMT, waktu);// input manual belum pakai rtc
  int jam, menit; 
  byte i = 0 ;
  Serial.println();
    for( byte j= 0 ; j<7; j++){ 
      get_float_time_parts(waktu[j], jam, menit); 
      int Min = menit + Ihtiyat;
      if (Min > 60){
        Min = Min - 60;
        jam + 1;
      }
      if (j == 0){
        // Serial.printf(NamaWaktu[i]);
        // Serial.printf(" : ");
        // Serial.print(jam);
        Sjam[i] = jam;
        // Serial.printf(":");
        // Serial.println(Min-10);
        Smin[i] = Min-10;
        // Serial.println("");
        i++;
        // Serial.printf(NamaWaktu[i]);
        // Serial.printf(" : ");
        // Serial.print(jam);
        Sjam[i] = jam;
        // Serial.printf(":");
        // Serial.println(Min);
        Smin[i] = Min;
        // Serial.println("");
      } else {
        // Serial.printf(NamaWaktu[i]);
        // Serial.printf(" : ");
        // Serial.print(jam);
        Sjam[i] = jam;
        // Serial.printf(":");
        // Serial.println(Min);
        Smin[i] = Min;
        // Serial.println("");
      }
        i++;
     }  
     
     jadwal_lain();
     delay(1000);
}
