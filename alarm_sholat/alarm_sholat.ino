/*............................................................................................................*/
#define Ihtiyat 2  // sebagai waktu ihtiyat (untuk berhati-hati)
#define buzzerPin 5
#define sensorPin D4
#define RX D2
#define TX D3

#include <TimeLib.h>
#include "PrayerTimes.h"
#include "RTClib.h"
#include "Arduino.h"
#include "Wire.h"
#include <DFPlayerMini_Fast.h>
#include <SoftwareSerial.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

SoftwareSerial mySerial(RX, TX);  // RX, TX

DFPlayerMini_Fast myMP3;

double waktu[sizeof(TimeName) / sizeof(char *)];  //0.00

static const char *NamaWaktu [] ={ //array untuk waktu-waktu shalat & alarm
 "Imsak" ,      //0
 "Subuh" ,      //1
 "Sunrise" ,    //2 
 "Dzuhur" ,     //3
 "Ashar" ,      //4
 "Sunset" ,     //5
 "Maghrib" ,    //6
 "Isya'"  ,      //7
 "Bangun_Subuh",  //8
 "Sedekah_Subuh", //9
 "Piket_Pagi",    //10
 "Piket_Sore",    //11
 "Waktu_Belajar", //12
 "Waktu_Tidur"    //13
};


int dayR;
int monthR;
int yearR;
int jamR;
int minR;
int secR;
int epoch;
byte lock_alarm = 0;
int counter;

unsigned long previousMillis = 0;
const long interval = 1000;

//RTC
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Juma'at", "Sabtu" };
DateTime real;
DateTime future;

int Sjam[8];  //penyimpanan jam sholat
int Smin[8];  //penyimpanan menit sholat

const byte JUMLAH_WAKTU = sizeof(NamaWaktu) / sizeof(char *);  // = jumlah waktu (9)
                                                               // pengaturan bujur lintang
float Latitude = -7.5543f;                                     // lintang
float Longitude = 110.8270f;                                   // bujur
int GMT = 7;                                                   // zona waktu WIB=7, WITA=8, WIT=9
// -6.184499, 106.827153 (jakarta pusat)
// -7.554301, 110.827064 (solo MBZ)

void initRTC() {
  // if (!rtc.begin()) {
  //   Serial.println("Couldn't find RTC");
  //   Serial.flush();
  //   abort();
  // }
  rtc.begin();
  // if (rtc.lostPower()) {
  //   Serial.println("RTC lost power, let's set the time!");
  //   rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // }
  // if (Serial) {
  //   Serial.println("SerialConnect");
  //   rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // }
}

void RTC_now() {
  Serial.println("-------------------------------------------------------------------");
  DateTime real = rtc.now();
  Serial.print(real.year(), DEC);
  yearR = real.year(), DEC;
  Serial.print('/');
  Serial.print(real.month(), DEC);
  monthR = real.month(), DEC;
  Serial.print('/');
  Serial.print(real.day(), DEC);
  dayR = real.day(), DEC;
  Serial.print(" (");
  Serial.print(daysOfTheWeek[real.dayOfTheWeek()]);
  Serial.print(") ");
  Serial.print(real.hour(), DEC);
  jamR = real.hour(), DEC;
  Serial.print(':');
  Serial.print(real.minute(), DEC);
  minR = real.minute(), DEC;
  Serial.print(':');
  Serial.print(real.second(), DEC);
  secR = real.second(), DEC;
  Serial.println();

  // Serial.print("Temperature: ");
  // Serial.print(real.getTemperature());
  // Serial.println(" C");
  epoch = real.unixtime();
  Serial.println(epoch);
  Serial.println("-------------------------------------------------------------------");
}

void GetPrayerTimes() {
  get_prayer_times(2023, 8, 31, Latitude, Longitude, GMT, waktu);  // input manual belum pakai rtc
  int jam, menit;
  byte i = 0;
  Serial.println();
  for (byte j = 0; j < 7; j++) {
    get_float_time_parts(waktu[j], jam, menit);
    int Min = menit + Ihtiyat;
    if (Min > 60) {
      Min = Min - 60;
      jam + 1;
    }
    if (j == 0) {
      // Serial.printf(NamaWaktu[i]);
      // Serial.printf(" : ");
      // Serial.print(jam);
      Sjam[i] = jam;
      // Serial.printf(":");
      // Serial.println(Min-10);
      Smin[i] = Min - 10;
      // Serial.println("");
      i++;
      // Serial.printf(NamaWaktu[i]);
      // Serial.printf(" : ");
      // Serial.print(jam);
      Sjam[i] = jam;
      // Serial.printf(":");
      // Serial.println(Min);
      Smin[i] = Min;
      // Serial.println("");
    } else {
      // Serial.printf(NamaWaktu[i]);
      // Serial.printf(" : ");
      // Serial.print(jam);
      Sjam[i] = jam;
      // Serial.printf(":");
      // Serial.println(Min);
      Smin[i] = Min;
      // Serial.println("");
    }
    i++;
  }
  delay(10);
}

void jadwal_lain() {
  // 15 menit sebelum subuh [8]
  Sjam[8] = Sjam[1];
  Smin[8] = Smin[1] - 15;  //menit subuh dikurangi 15 menit
  if (Smin[8] < 0) {
    Sjam[8] = Sjam[8] - 1;
    Smin[8] = Smin[8] + 60;
  }
  // Sedekah subuh 10 menit sebelum subuh [9]
  Sjam[9] = Sjam[1];
  Smin[9] = Smin[1] - 10;  //menit subuh dikurangi 10 menit
  if (Smin[9] < 0) {
    Sjam[9] = Sjam[8] - 1;
    Smin[9] = Smin[8] + 60;
  }
  // piket pagi 05:30 [10]
  Sjam[10] = 5;
  Smin[10] = 30;
  // piket sore 16.30 [11]
  Sjam[11] = 16;
  Smin[11] = 30;
  // waktu belajar 19:30 [12]
  Sjam[12] = 19;
  Smin[12] = 30;
  // waktu tidur 21:30  [13]
  Sjam[13] = 21;
  Smin[13] = 30;
}

void cek_alarm() {
  RTC_now();

  // bangun subuh
  if (jamR == Sjam[8] && minR == Smin[8]) {
    if (lock_alarm = 0) {
      bangun_subuh();
      lock_alarm = 1;
    }
  } else
    // sedekah subuh
    if (jamR == Sjam[9] && minR == Smin[9]) {
      if (lock_alarm = 0) {
        sedekah_subuh();
        lock_alarm = 1;
      }
    } else
      // sholat subuh
      if (jamR == Sjam[1] && minR == Smin[1]) {
        if (lock_alarm = 0) {
          sholat_subuh();
          lock_alarm = 1;
        }
      } else
        // piket pagi
        if (jamR == Sjam[10] && minR == Smin[10]) {
          if (lock_alarm = 0) {
            piket_pagi();
            lock_alarm = 1;
          }
        } else
          // piket sore
          if (jamR == Sjam[11] && minR == Smin[11]) {
            if (lock_alarm = 0) {
              piket_sore();
              lock_alarm = 1;
            }
          } else
            // sholat magrib
            if (jamR == Sjam[6] && minR == Smin[6]) {
              if (lock_alarm = 0) {
                sholat_magrib();
                lock_alarm = 1;
              }
            } else
              // sholat isya
              if (jamR == Sjam[7] && minR == Smin[7]) {
                if (lock_alarm = 0) {
                  sholat_isya();
                  lock_alarm = 1;
                }
              } else
                // belajar
                if (jamR == Sjam[12] && minR == Smin[12]) {
                  if (lock_alarm = 0) {
                    belajar();
                    lock_alarm = 1;
                  }
                } else
                  // tidur
                  if (jamR == Sjam[13] && minR == Smin[13]) {
                    if (lock_alarm = 0) {
                      tidur();
                      lock_alarm = 1;
                    }
                  } else {
                    lock_alarm = 0;
                  }
}

void bangun_subuh() {
  Serial.println("========== bangun subuh ================");
  //myMP3.play();
}
void sedekah_subuh() {
  Serial.println("========== sedekah subuh ================");
  //myMP3.play();
  while (counter < 10) {  // Membaca sensor hingga 10 kali
    // Membaca status sensor proximity
    bool isObjectDetected = digitalRead(sensorPin);

    if (isObjectDetected) {
      Serial.println("Objek terdeteksi!");
      digitalWrite(buzzerPin, HIGH);
      delay(200);
      digitalWrite(buzzerPin, LOW);

    } else {
      Serial.println("Tidak ada objek.");
    }

    counter++;    // Menambahkan counter setiap kali membaca sensor
    delay(1000);  // Tunggu 1 detik sebelum membaca ulang
  }
}
void sholat_subuh() {
  Serial.println("========== sholat subuh ================");
  //myMP3.play();
}
void piket_pagi() {
  Serial.println("========== piket pagi ================");
  //myMP3.play();
}
void piket_sore() {
  Serial.println("========== piket sore ================");
  //myMP3.play();
}
void sholat_magrib() {
  Serial.println("========== sholat magrib ================");
  //myMP3.play();
}
void sholat_isya() {
  Serial.println("========== sholat isya' ================");
  //myMP3.play();
}
void belajar() {
  Serial.println("========== belajar ================");
  //myMP3.play();
}
void tidur() {
  Serial.println("========== tidur ================");
  //myMP3.play();
}

void HomeDisplay() {

    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print(jamR, DEC);
    lcd.print(':');
    lcd.print(minR, DEC);
    GetPrayerTimes();
    jadwal_lain();
  
}


void setup() {
  Serial.begin(9600);
  //prayer
  set_calc_method(ISNA);                    // metode perhitungan
  set_asr_method(Shafii);                   // madzhab?
  set_high_lats_adjust_method(AngleBased);  // Adjusting Methods for Higher Latitudes
  set_fajr_angle(20);                       // sudut waktu subuh
  set_isha_angle(18);                       // sudut waktu isyak
  //dfplayer
  mySerial.begin(9600);
  // myMP3.begin(mySerial, true);
  // Serial.println("Setting volume to max");
  // myMP3.volume(30);

  // initRTC();
  // pinMode(sensorPin, INPUT);
  // pinMode(buzzerPin, OUTPUT);

  // lcd.init();
  // lcd.backlight();
}

void loop() {
  Serial.println("loop");
  HomeDisplay();
  cek_alarm();
  for (byte i = 0; i < 14; i++) {
    Serial.print(NamaWaktu[i]);
    Serial.print(" = ");
    Serial.print(Sjam[i]);
    Serial.print(":");
    Serial.println(Smin[i]);
    delay(10);
  }
  delay(100);
}
